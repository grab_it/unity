﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public class Manager : MonoBehaviour {

	public static Manager instance;
	private Product[] products;
	private ProductStand[] productStands;
	private int activeProductIndex = 0;
	private Product activeProduct = null;
	private string[] productNames = new string[] {
		"Camera",
		"Helmet",
		// "SpaceShip",
		// "RetroTV",
		// "AlarmClock",
		"PlantSmall",
		// "Microwave",
		// "Mouse",
		// "Chicken",
		// "Sofa",
		// "Grenade",
		// "Teddy",
		"Elephant"
	};

	private string[] productsRequiringStand = new string[] {
		"PlantSmall",
		// "Teddy"
	};

	void Awake () {
		instance = this;
	}

	// Use this for initialization
	void Start () {
		products = Resources.FindObjectsOfTypeAll (typeof (Product)) as Product[];
		productStands = Resources.FindObjectsOfTypeAll (typeof (ProductStand)) as ProductStand[];
		Debug.Log ("<color=red>" + products.Length + " products found</color>");
		SetActiveProduct (activeProductIndex);
	}

	private void SetActiveProduct (int index) {
		if (products.Length == 0 || index >= productNames.Length) {
			return;
		}
		string activeProductName = productNames[index];
		for (int i = 0; i < products.Length; i++) {
			Product product = products[i];
			string productName = product.gameObject.name;
			if (productName == activeProductName) {
				// Debug.Log ("Set " + productName + " to active");
				product.gameObject.SetActive (true);
				activeProduct = product;
				activeProductIndex = index;
				ShowProductStand (productsRequiringStand.Contains (productName));
			} else {
				// Debug.Log ("Set " + productName + " to inactive");
				product.gameObject.SetActive (false);
			}
		}
	}
	private void ShowProductStand (bool show) {
		foreach (ProductStand stand in productStands) {
			stand.gameObject.SetActive (show);
		}
	}

	// Update is called once per frame
	void Update () {

	}
	public void PreviousProduct () {
		// Debug.Log ("Previous Product");
		activeProductIndex--;
		if (activeProductIndex < 0) {
			activeProductIndex = productNames.Length - 1;
		}
		SetActiveProduct (activeProductIndex);
	}
	public void NextProduct () {
		// Debug.Log ("Next Product");
		activeProductIndex++;
		if (activeProductIndex >= productNames.Length) {
			activeProductIndex = 0;
		}
		SetActiveProduct (activeProductIndex);
	}

}