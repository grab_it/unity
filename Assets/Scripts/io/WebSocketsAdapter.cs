﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using BestHTTP.WebSocket;
using UnityEngine;

public class WebSocketsAdapter : MonoBehaviour {

	public string server = "192.168.170.131";
	public int port = 4120;
	[Tooltip ("Retry to connect to Bridge every n seconds")]
	public int reconnectTimeout = 5;

	private WebSocket ws;
	private bool connected = false;
	private Manager manager;

	void Start () {
		manager = Manager.instance;
		StartCoroutine ("AutoConnectTimer");
	}

	void OnApplicationPause (bool paused) {
		if (paused) {
			CloseConnection ();
		} else {
			OpenConnection (); // This will also open the first connection on startup
		}
	}

	void OnApplicationQuit () {
		CloseConnection ();
	}

	void OpenConnection () {
		string url = "ws://" + server + ":" + port;
		ws = new WebSocket (new System.Uri (url));
		ws.OnOpen += OnWebSocketOpen;
		ws.OnMessage += OnMessageReceived;
		ws.OnError += OnWebSocketError;
		ws.Open ();
	}

	void CloseConnection () {
		this.connected = false;
		ws.Close ();
	}

	void RestartConnection () {
		if (connected) {
			CloseConnection ();
		}
		OpenConnection ();
	}

	IEnumerator AutoConnectTimer () {
		while (true) {
			yield return new WaitForSeconds (reconnectTimeout);
			if (!this.connected) {
				Debug.Log ("Trying to connect to server.");
				OpenConnection ();
			}
		}
	}

	private void OnWebSocketOpen (WebSocket ws) {
		this.connected = true;
	}

	private void OnWebSocketError (WebSocket ws, Exception ex) {
		if (ex != null) {
			Debug.Log (ex);
		}
	}

	// Send request to server via Websocket
	public void Send (string message) {
		if (connected) {
			ws.Send (message);
		}
	}

	// Process data received from the server
	private void OnMessageReceived (WebSocket webSocket, string message) {
		JSONObject json = new JSONObject (message);
		string messageType = json["type"].str;
		switch (messageType) {
			case "pose":
				Debug.Log ("<color=red>Message from Server</color>");
				string pose = json["pose"].str;
				switch (pose) {
					case "double_tap":
						if (manager) {
							manager.NextProduct ();
						}
						break;
				}
				break;
		}
	}

	public void SetServer (string server, int port) {
		int newPort = port == 0 ? this.port : port;
		if (this.server != server || this.port != newPort) {
			this.server = server;
			this.port = newPort;
			RestartConnection ();
		}
	}
}