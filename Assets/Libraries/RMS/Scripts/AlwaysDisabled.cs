using UnityEngine;
using System.Collections;

public class AlwaysDisabled : MonoBehaviour
{
    private void OnEnable()
    {
        this.gameObject.SetActive(false);
    }
}
