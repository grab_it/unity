using UnityEngine;
using UnityEngine.Events;
using UnityEngine.EventSystems;

public class ExecuteOnTap_WithinBoundaries : MonoBehaviour, IPointerDownHandler
{
    public new bool enabled = true;

    public enum ClickPositions
    {
        Inside = 0,
        Outside
    };
    public ClickPositions clickPosition = ClickPositions.Inside;

    public float xMin = 0f;
    public float xMax = 1f;
    public float yMin = 0f;
    public float yMax = 1f;

    public UnityEvent eventToExecute;

    public virtual void OnPointerDown(PointerEventData eventData)
    {
        if (enabled)
        {
            float width = Screen.width * xMax - Screen.width * xMin;
            float height = Screen.height * yMax - Screen.height * yMin;

            Vector2 rectPosition = new Vector2(Screen.width * xMin, Screen.height * yMin);
            Vector2 rectSize = new Vector2(width, height);
            Rect screenArea = new Rect(rectPosition, rectSize);

            bool isInside = screenArea.Contains(eventData.pressPosition);

            if (clickPosition == ClickPositions.Inside && isInside)
                eventToExecute.Invoke();
            else if (clickPosition == ClickPositions.Outside && !isInside)
                eventToExecute.Invoke();
        }
    }
}
