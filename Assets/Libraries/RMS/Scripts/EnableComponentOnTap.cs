using UnityEngine;
using System.Linq;

public class EnableComponentOnTap : MonoBehaviour
{
    public MonoBehaviour componentToEnable;
    private Ray ray;

    private void Update()
    {
        if (componentToEnable == null || componentToEnable.enabled || Camera.main == null)
            return;

        if (Input.GetMouseButtonDown(0))    // clicked
        {
            ray = Camera.main.ScreenPointToRay(Input.mousePosition);

            // check if the place we tap sends a ray through this object
            if (Physics.RaycastAll(ray, float.PositiveInfinity, 1 << gameObject.layer).Any(t => t.transform == transform))
            {
                componentToEnable.enabled = true;
            }
        }
    }
}
